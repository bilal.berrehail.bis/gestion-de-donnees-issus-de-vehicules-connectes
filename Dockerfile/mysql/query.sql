--
-- Create user
--
CREATE USER 'bilal'@'%' IDENTIFIED by '1234';GRANT ALL PRIVILEGES ON *.* TO 'bilal'@'%' REQUIRE NONE WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;GRANT ALL PRIVILEGES ON `bilal\_%`.* TO 'bilal'@'%';
--
-- Create database
--
CREATE DATABASE iot;
--
-- Structure de la table `table_cam`
--
use iot;
DROP TABLE IF EXISTS `table_cam`;
CREATE TABLE IF NOT EXISTS `table_cam` (
  `id` int(10) NOT NULL,
  `type` int(10) NOT NULL,
  `vitesse` int(10) NOT NULL,
  `heading` int(10) NOT NULL,
  `latitude` double(10,3) NOT NULL,
  `longitude` double(10,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
--
-- Structure de la table `table_denm`
--
DROP TABLE IF EXISTS `table_denm`;
CREATE TABLE IF NOT EXISTS `table_denm` (
  `id` int(10) NOT NULL,
  `type` int(10) NOT NULL,
  `cause` int(10) NOT NULL,
  `sub_cause` int(10) NOT NULL,
  `latitude` double(10,3) NOT NULL,
  `longitude` double(10,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
COMMIT;