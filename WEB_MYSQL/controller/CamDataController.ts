import {Request, Response} from "express";
import {CAM} from "../model/CamModel";
import {TypeCountModel} from "../model/TypeCountModel";

const mysql = require('mysql');

const con = mysql.createConnection({
    host: "127.0.0.1",
    user: "bilal",
    password: "1234",
    database: "iot"
});

export class CamDataController {
    public static get(req: Request, res: Response, next: any) {
        con.query("SELECT * FROM table_cam", function (err, list) {
            if (err) throw err;
            let cams: Array<CAM> = JSON.parse(JSON.stringify(list))

            con.query("SELECT AVG(VITESSE) AS average FROM table_cam", function (err, result) {
                if (err) throw err;
                let average_speed: number = JSON.parse(JSON.stringify(result[0])).average;

                con.query("SELECT COUNT(*) as count FROM (SELECT * FROM table_cam GROUP BY id) AS z", function (err, result) {
                    if (err) throw err;
                    let count: number = JSON.parse(JSON.stringify(result[0])).count;

                    con.query("SELECT type, COUNT(*) as count FROM (SELECT id, type, COUNT(*) FROM table_cam GROUP BY id, type) as z group by type", function (err, list) {
                        if (err) throw err;
                        let typeCounts: Array<TypeCountModel> = JSON.parse(JSON.stringify(list))

                        let count_ordinaire = 0;
                        let count_urgence = 0;
                        let count_routier = 0;

                        typeCounts.forEach(function (type_count) {
                            switch (type_count.type) {
                                case TypeCountModel.ORDINAIRE:
                                    count_ordinaire = type_count.count;
                                    break;
                                case TypeCountModel.URGENCE:
                                    count_urgence = type_count.count;
                                    break;
                                case TypeCountModel.ROUTIER:
                                    count_routier = type_count.count;
                                    break;
                            }
                        });

                        res.json({
                            title: 'CAM\'s listing',
                            cams: cams,
                            average_speed: average_speed,
                            count: count,
                            count_ordinaire: count_ordinaire,
                            count_urgence: count_urgence,
                            count_routier: count_routier
                        })
                        return next();
                    });
                });
            });
        });
    }
}