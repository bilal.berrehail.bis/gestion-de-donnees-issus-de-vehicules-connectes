import {Request, Response} from 'express';

const mysql = require('mysql');
const request = require('request');

const con = mysql.createConnection({
    host: "127.0.0.1",
    user: "bilal",
    password: "1234",
    database: "iot"
});

export class DenmController {
    public static get(req: Request, res: Response, next: any) {
        request('http://localhost:3000/api/denm', {json: true}, (err, response, body) => {
            if (err) {
                return console.log(err);
            }
            res.render('denm', body);
            return next();
        });
    }
}