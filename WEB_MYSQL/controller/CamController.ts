import {Request, Response} from 'express';

const request = require('request');

export class CamController {
    public static get(req: Request, res: Response, next: any) {
        request('http://localhost:3000/api/cam', {json: true}, (err, response, body) => {
            if (err) {
                return console.log(err);
            }
            res.render('cam', body);
            return next();
        })
    }
}