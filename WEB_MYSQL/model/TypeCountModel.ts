export class TypeCountModel {
    public static readonly ORDINAIRE = 5;
    public static readonly URGENCE = 10;
    public static readonly ROUTIER = 15;

    type: number;
    count: number;
}
