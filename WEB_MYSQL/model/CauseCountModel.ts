export class CauseCountModel {
    public static readonly TRAVAUX = 3;
    public static readonly ACCIDENT = 4;
    public static readonly EMBOUTEILLAGE = 5;
    public static readonly ROUTE_GLISSANTE = 6;
    public static readonly BROUILLARD = 7;

    cause: number;
    count: number;
}