"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const CamController_1 = require("../controller/CamController");
const DenmController_1 = require("../controller/DenmController");
const CamDataController_1 = require("../controller/CamDataController");
const DenmDataController_1 = require("../controller/DenmDataController");
const app = express_1.default();
const port = 3000;
app.set('view engine', 'pug');
app.get('/', (req, res) => {
    res.send('The sedulous hyena ate the antelope!');
});
app.get('/cam', CamController_1.CamController.get);
app.get('/denm', DenmController_1.DenmController.get);
app.get('/api/cam', CamDataController_1.CamDataController.get);
app.get('/api/denm', DenmDataController_1.DenmDataController.get);
app.listen(port, () => {
    return console.log(`[ok] server is listening on ${port}`);
});
//# sourceMappingURL=app.js.map