"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const request = require('request');
class CamController {
    static get(req, res, next) {
        request('http://localhost:3000/api/cam', { json: true }, (err, response, body) => {
            if (err) {
                return console.log(err);
            }
            res.render('cam', body);
            return next();
        });
    }
}
exports.CamController = CamController;
//# sourceMappingURL=CamController.js.map