"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeCountModel_1 = require("../model/TypeCountModel");
const CauseCountModel_1 = require("../model/CauseCountModel");
const mysql = require('mysql');
const con = mysql.createConnection({
    host: "127.0.0.1",
    user: "bilal",
    password: "1234",
    database: "iot"
});
class DenmDataController {
    static get(req, res, next) {
        con.query("SELECT * FROM table_denm", function (err, list) {
            if (err)
                throw err;
            let denms = JSON.parse(JSON.stringify(list));
            con.query("SELECT COUNT(*) as count FROM (SELECT * FROM table_denm GROUP BY id) AS z", function (err, result) {
                if (err)
                    throw err;
                let count = JSON.parse(JSON.stringify(result[0])).count;
                con.query("SELECT type, COUNT(*) as count FROM (SELECT id, type, COUNT(*) FROM table_denm GROUP BY id, type) as z group by type", function (err, list) {
                    if (err)
                        throw err;
                    let typeCounts = JSON.parse(JSON.stringify(list));
                    let count_ordinaire = 0;
                    let count_urgence = 0;
                    let count_routier = 0;
                    typeCounts.forEach(function (type_count) {
                        switch (type_count.type) {
                            case TypeCountModel_1.TypeCountModel.ORDINAIRE:
                                count_ordinaire = type_count.count;
                                break;
                            case TypeCountModel_1.TypeCountModel.URGENCE:
                                count_urgence = type_count.count;
                                break;
                            case TypeCountModel_1.TypeCountModel.ROUTIER:
                                count_routier = type_count.count;
                                break;
                        }
                    });
                    con.query("SELECT cause, COUNT(*) as count FROM table_denm GROUP BY cause", function (err, list) {
                        if (err)
                            throw err;
                        let causeCounts = JSON.parse(JSON.stringify(list));
                        let count_travaux = 0;
                        let count_accident = 0;
                        let count_embouteillage = 0;
                        let count_route_glissante = 0;
                        let count_brouillard = 0;
                        causeCounts.forEach(function (causeCount) {
                            switch (causeCount.cause) {
                                case CauseCountModel_1.CauseCountModel.TRAVAUX:
                                    count_travaux = causeCount.count;
                                    break;
                                case CauseCountModel_1.CauseCountModel.ACCIDENT:
                                    count_accident = causeCount.count;
                                    break;
                                case CauseCountModel_1.CauseCountModel.EMBOUTEILLAGE:
                                    count_embouteillage = causeCount.count;
                                    break;
                                case CauseCountModel_1.CauseCountModel.ROUTE_GLISSANTE:
                                    count_route_glissante = causeCount.count;
                                    break;
                                case CauseCountModel_1.CauseCountModel.BROUILLARD:
                                    count_brouillard = causeCount.count;
                                    break;
                            }
                        });
                        res.json({
                            title: 'DENM\'s listing',
                            denms: denms,
                            count: count,
                            count_ordinaire: count_ordinaire,
                            count_urgence: count_urgence,
                            count_routier: count_routier,
                            count_travaux: count_travaux,
                            count_accident: count_accident,
                            count_embouteillage: count_embouteillage,
                            count_route_glissante: count_route_glissante,
                            count_brouillard: count_brouillard
                        });
                        return next();
                    });
                });
            });
        });
    }
}
exports.DenmDataController = DenmDataController;
//# sourceMappingURL=DenmDataController.js.map