"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CauseCountModel {
}
CauseCountModel.TRAVAUX = 3;
CauseCountModel.ACCIDENT = 4;
CauseCountModel.EMBOUTEILLAGE = 5;
CauseCountModel.ROUTE_GLISSANTE = 6;
CauseCountModel.BROUILLARD = 7;
exports.CauseCountModel = CauseCountModel;
//# sourceMappingURL=CauseCountModel.js.map