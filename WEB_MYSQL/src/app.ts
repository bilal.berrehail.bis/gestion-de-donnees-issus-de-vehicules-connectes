import express from 'express';
import {CamController} from "../controller/CamController";
import {DenmController} from "../controller/DenmController";
import {CamDataController} from "../controller/CamDataController";
import {DenmDataController} from "../controller/DenmDataController";

const app = express();
const port = 3000;

app.set('view engine', 'pug');

app.get('/', (req, res) => {
    res.send('Work!');
});

app.get('/cam', CamController.get);
app.get('/denm', DenmController.get);
app.get('/api/cam', CamDataController.get);
app.get('/api/denm', DenmDataController.get);

app.listen(port, () => {
    return console.log(`[ok] server is listening on ${port}`);
});

