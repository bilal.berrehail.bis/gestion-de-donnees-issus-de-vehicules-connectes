package com.iot;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.jid.parts.Resourcepart;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class App {
    private static final String QUEUE_CAM = "QUEUE_CAM";
    private static final String QUEUE_DENM = "QUEUE_DENM";

    private static MultiUserChat multiUserChat;

    public static void main(String[] argv) {
        initializeXmppClient();
        initializeRabbitMq();
    }

    private static void initializeXmppClient() {
        try {
            EntityBareJid mucJid = (EntityBareJid) JidCreate.bareFrom("main_room@conference.desktop-gnpr3b8");

            XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration
                    .builder()
                    .setSecurityMode(SecurityMode.disabled)
                    .setUsernameAndPassword("bilal", "1234")
                    .setXmppDomain("192.168.1.42")
                    .build();

            AbstractXMPPConnection connection = new XMPPTCPConnection(config);
            connection.connect();

            connection.login();

            MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(
                    connection
            );
            multiUserChat = manager.getMultiUserChat(mucJid);
            Resourcepart resourcepart = Resourcepart.from("Robot-MQ_XMPP-" + UUID.randomUUID().getMostSignificantBits());
            multiUserChat.join(resourcepart);
            System.out.println("-- [READY] Client XMPP --");
        } catch (Exception ex) {
            System.out.println("initializeXmppClient -> " + ex.toString());
        }
    }

    private static void initializeRabbitMq() {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUsername("bilal");
            factory.setPassword("1234");
            factory.setHost("192.168.1.42");
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.queueDeclare(QUEUE_CAM, false, false, false, null);
            channel.queueDeclare(QUEUE_DENM, false, false, false, null);

            System.out.println("-- Mise en écoute des messages --");

            channel.basicConsume(QUEUE_CAM, true, (d, message) -> {
                String msgFormatted = new String(message.getBody(), StandardCharsets.UTF_8);
                System.out.println("[CAM] Message reçu : " + msgFormatted);
                try {
                    multiUserChat.sendMessage(msgFormatted);
                } catch (Exception ex) {
                    System.out.println("initializeRabbitMq::sendMessage::CAM -> " + ex.toString());
                }
            }, (CancelCallback) null);
            channel.basicConsume(QUEUE_DENM, true, (d, message) -> {
                String msgFormatted = new String(message.getBody(), StandardCharsets.UTF_8);
                System.out.println("[DENM] Message reçu : " + msgFormatted);
                try {
                    multiUserChat.sendMessage(msgFormatted);
                } catch (Exception ex) {
                    System.out.println("initializeRabbitMq::sendMessage::DENM -> " + ex.toString());
                }
            }, (CancelCallback) null);
        } catch (Exception ex) {
            System.out.println("initializeRabbitMq -> " + ex.toString());
        }
    }
}
