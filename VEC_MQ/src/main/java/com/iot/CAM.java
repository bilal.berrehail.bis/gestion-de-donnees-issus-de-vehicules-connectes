package com.iot;

public class CAM {
    private Integer id;
    private int type;
    private int speed;
    private int heading;
    private double latitude;
    private double longitude;

    public CAM(String packet) {
        try {
            String[] packetSplit = packet.split("[-]");
            this.id = Integer.parseInt(packetSplit[1]);
            this.type = Integer.parseInt(packetSplit[2]);
            this.speed = Integer.parseInt(packetSplit[3]);
            this.heading = Integer.parseInt(packetSplit[4]);
            this.latitude = Double.parseDouble(packetSplit[5]);
            this.longitude = Double.parseDouble(packetSplit[6]);
        } catch (Exception ex) {
            this.id = null;
        }
    }

    public Integer getId() {
        return id;
    }

    public int getType() {
        return type;
    }

    public int getSpeed() {
        return speed;
    }

    public int getHeading() {
        return heading;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
