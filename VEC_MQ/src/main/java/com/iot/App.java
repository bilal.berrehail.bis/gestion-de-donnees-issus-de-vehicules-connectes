package com.iot;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class App {
    private static final String QUEUE_CAM = "QUEUE_CAM";
    private static final String QUEUE_DENM = "QUEUE_DENM";

    private static int vec_id;
    private static int vec_type;

    private static Channel channel;

    public static void main(String[] args) {
        initializeRabbitMq();
    }

    private static void initializeRabbitMq() {
        vec_id = new Random().nextInt(5000) + 1000;
        vec_type = new int[]{5, 10, 15}[new Random().nextInt(2)];

        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUsername("bilal");
            factory.setPassword("1234");
            factory.setHost("192.168.1.42");
            Connection connection = factory.newConnection();
            channel = connection.createChannel();

            channel.queueDeclare(QUEUE_CAM, false, false, false, null);
            channel.queueDeclare(QUEUE_DENM, false, false, false, null);

            System.out.println("-- [RabbitMQ] Vous pouvez dès à présent envoyer vos packets --");

            sendCamDelayed(DELAY_INF_90);
            sendDenmDelayed(new Random().nextInt(10) + 10);

            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNext()) {
                scanner.next();
            }
            scanner.close();
        } catch (Exception ex) {
            System.out.println("initializeRabbitMq -> " + ex.toString());
        }
    }

    private static final int DELAY_SUP_90 = 100; // ms
    private static final int DELAY_INF_90 = 1000; // ms

    private static void sendCamDelayed(int delay) {
        ScheduledExecutorService scheduler
                = Executors.newSingleThreadScheduledExecutor();

        scheduler.schedule(App::generateAndSendCAMMessage, delay, TimeUnit.MILLISECONDS);
        scheduler.shutdown();
    }

    private static void sendDenmDelayed(int delay) {
        ScheduledExecutorService scheduler
                = Executors.newSingleThreadScheduledExecutor();

        scheduler.schedule(App::generateAndSendDENMMessage, delay, TimeUnit.SECONDS);
        scheduler.shutdown();
    }

    private static void generateAndSendDENMMessage() {
        int cause = new Random().nextInt(4) + 3;
        int sub_cause = 0; // default
        double latitude = 3 + (4 - 3) * new Random().nextDouble();
        double longitude = 3 + (4 - 3) * new Random().nextDouble();

        String denmMessage = "1-" + vec_id + "-" + vec_type + "-" + cause + "-" + sub_cause + "-" +
                latitude + "-" + longitude;

        try {
            if (handleMessageToSend(denmMessage) == DENM) {
                channel.basicPublish("", QUEUE_DENM, null, denmMessage.getBytes());
                System.out.println("-- [DENM] Message envoyé --");
            }
        } catch (Exception ex) {
            System.out.println("generateAndSendDENMMessage -> " + ex.toString());
        }

        sendDenmDelayed(new Random().nextInt(10) + 10);
    }

    private static void generateAndSendCAMMessage() {
        int speed = new Random().nextInt(140) + 20;
        int heading = new Random().nextInt(360);
        double latitude = 3 + (4 - 3) * new Random().nextDouble();
        double longitude = 3 + (4 - 3) * new Random().nextDouble();
        String camMessage = "0-" + vec_id + "-" + vec_type + "-" + speed + "-" + heading + "-" +
                latitude + "-" + longitude;

        try {
            if (handleMessageToSend(camMessage) == CAM) {
                channel.basicPublish("", QUEUE_CAM, null, camMessage.getBytes());
                System.out.println("-- [CAM] Message envoyé --");
            }
        } catch (Exception ex) {
            System.out.println("generateAndSendCAMMessage -> " + ex.toString());
        }

        sendCamDelayed(speed >= 90 ? DELAY_SUP_90 : DELAY_INF_90);
    }

    private static final int FAILED = 0;
    private static final int CAM = 1;
    private static final int DENM = 2;

    private static int handleMessageToSend(String message) {
        try {
            String[] messageSplit = message.split("[-]");
            if (messageSplit.length > 0) {
                if (messageSplit[0].equals("0")) {
                    CAM cam = new CAM(message);
                    if (cam.getId() != null) {
                        return CAM;
                    } else {
                        System.out.println("-- Annulation de l'envoi d'un mauvais message [id null] --");
                    }
                } else if (messageSplit[0].equals("1")) {
                    DENM denm = new DENM(message);
                    if (denm.getId() != null) {
                        return DENM;
                    }
                } else {
                    System.out.println("-- Annulation de l'envoi d'un mauvais message [cam or denm not recognized] --");
                }
            } else {
                System.out.println("-- Annulation de l'envoi mauvais message [message too short] --");
            }
        } catch (Exception ex) {
            System.out.println("handleMessageToSend -> " + ex.toString());
        }
        return FAILED;
    }
}
