package com.iot;

public class DENM {
    private Integer id;
    private int type;
    private int cause;
    private int subCause;
    private Double latitude;
    private Double longitude;

    public DENM(String packet) {
        try {
            String[] packetSplit = packet.split("[-]");
            this.id = Integer.parseInt(packetSplit[1]);
            this.type = Integer.parseInt(packetSplit[2]);
            this.cause = Integer.parseInt(packetSplit[3]);
            this.subCause = Integer.parseInt(packetSplit[4]);
            this.latitude = Double.parseDouble(packetSplit[5]);
            this.longitude = Double.parseDouble(packetSplit[6]);
        } catch (Exception ex) {
            this.id = null;
        }
    }

    public Integer getId() {
        return id;
    }

    public int getType() {
        return type;
    }

    public int getCause() {
        return cause;
    }

    public int getSubCause() {
        return subCause;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
}
