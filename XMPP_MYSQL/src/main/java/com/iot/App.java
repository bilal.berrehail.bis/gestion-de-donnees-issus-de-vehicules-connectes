package com.iot;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.muc.MucEnterConfiguration;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.jid.parts.Resourcepart;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Locale;
import java.util.Scanner;
import java.util.UUID;

public class App {

    private static Connection connectionMysql;

    public static void main(String[] args) {
        initializeMysql();
        initializeXmppClient();

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            scanner.next();
        }
        scanner.close();
    }

    private static void initializeMysql() {
        try {
            connectionMysql = DriverManager.getConnection("jdbc:mysql://192.168.1.42:3306/iot?serverTimezone=UTC", "bilal", "1234");
        } catch (Exception ex) {
            System.out.println("initializeMysql -> " + ex.toString());
        }
    }

    private static void initializeXmppClient() {
        try {
            EntityBareJid mucJid = (EntityBareJid) JidCreate.bareFrom("main_room@conference.desktop-gnpr3b8");

            XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration
                    .builder()
                    .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
                    .setUsernameAndPassword("bilal", "1234")
                    .setXmppDomain("192.168.1.42")
                    .build();

            AbstractXMPPConnection connection = new XMPPTCPConnection(config);
            connection.connect();

            connection.login();

            MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(
                    connection
            );
            MultiUserChat multiUserChat = manager.getMultiUserChat(mucJid);
            multiUserChat.addMessageListener(message -> handleMessageReceived(message.getBody()));
            Resourcepart resourcepart = Resourcepart.from("Robot-MQ_XMPP-" + UUID.randomUUID().getMostSignificantBits());

            MucEnterConfiguration.Builder mec = multiUserChat.getEnterConfigurationBuilder(resourcepart);
            mec.requestMaxStanzasHistory(0);

            multiUserChat.join(mec.build());

            System.out.println("-- [READY] Client XMPP --");
        } catch (Exception ex) {
            System.out.println("initializeXmppClient -> " + ex.toString());
        }
    }

    private static void handleMessageReceived(String message) {
        try {
            String[] messageSplit = message.split("[-]");
            if (messageSplit.length > 0) {
                Statement statement = connectionMysql.createStatement();

                if (messageSplit[0].equals("0")) {
                    CAM cam = new CAM(message);
                    if (cam.getId() != null) {
                        String query = String.format(Locale.US, "INSERT INTO table_cam (id, type, vitesse, heading, latitude, longitude) VALUES (%d, %d, %d, %d, %.3f, %.3f)",
                                cam.getId(),
                                cam.getType(),
                                cam.getSpeed(),
                                cam.getHeading(),
                                cam.getLatitude(),
                                cam.getLongitude());

                        statement.execute(query);
                    } else {
                        System.out.println("-- Réception d'un mauvais message [id null] --");
                        return;
                    }
                } else if (messageSplit[0].equals("1")) {
                    DENM denm = new DENM(message);
                    if (denm.getId() != null) {
                        String query = String.format(Locale.US, "INSERT INTO table_denm (id, type, cause, sub_cause, latitude, longitude) VALUES (%d, %d, %d, %d, %.3f, %.3f)",
                                denm.getId(),
                                denm.getType(),
                                denm.getCause(),
                                denm.getSubCause(),
                                denm.getLatitude(),
                                denm.getLongitude());

                        statement.execute(query);
                    }
                } else {
                    System.out.println("-- Réception d'un mauvais message [cam or denm not recognized] --");
                    return;
                }

                System.out.println("-- Requête réussie --");
            } else {
                System.out.println("-- Réception d'un mauvais message [message too short] --");
            }
        } catch (Exception ex) {
            System.out.println("handleMessageReceived -> " + ex.toString());
        }
    }
}